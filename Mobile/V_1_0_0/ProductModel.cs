﻿namespace ProductsApi.Mobile.V_1_0_0;

public record ProductModel
(
    Guid Id,
    string Title,
    string Subtitle,
    string Price,
    string Description,
    string Image
);