﻿namespace ProductsApi.Mobile.V_1_0_0;

/// <summary>
/// Service providing product data
/// </summary>
public interface IProductService
{
    /// <summary>
    /// Gets list of available products
    /// </summary>
    /// <returns>List of available products</returns>
    Task<List<ProductModel>> GetProducts();
}