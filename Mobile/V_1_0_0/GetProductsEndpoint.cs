﻿using Ardalis.ApiEndpoints;
using Asp.Versioning;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace ProductsApi.Mobile.V_1_0_0;

public class GetProductsEndpoint : EndpointBaseAsync
    .WithoutRequest
    .WithActionResult<List<ProductModel>>
{
    private readonly IProductService _productService;

    public GetProductsEndpoint(IProductService service)
    {
        _productService = service;
    }

    [ApiVersion("1.0")]
    [HttpGet("Mobile/Products/v1.0")]
    [SwaggerOperation(Summary = "Gets all available products",
        Description = "Gets all available product data",
        OperationId = "Products.GetAll",
        Tags = new[] { "Product Endpoints" })]
    public override async Task<ActionResult<List<ProductModel>>> HandleAsync(CancellationToken cancellationToken = default)
    {
        var products = await _productService.GetProducts();
        return Ok(products);
    }
}

