﻿using System;

namespace ProductsApi.Mobile.V_1_0_0
{
    /// <inheritdoc />
    /// <summary>
    /// Service providing available product data
    /// </summary>
    public class ProductService : IProductService
    {
        public ProductService()
        {
        }

        /// <inheritdoc />
        public async Task<List<ProductModel>> GetProducts()
        {
            await Task.Yield();
            Guid productId1 = Guid.Parse("697b009c-e03e-4563-83e7-a7286d74f02f");
            Guid productId2 = Guid.Parse("48815ebf-be4a-409a-96aa-15d92cceece1");
            Guid productId3 = Guid.Parse("2b65ab9a-f400-45f2-9e80-22a37e99cfc9");
            Guid productId4 = Guid.Parse("fddf48d8-2be5-41f5-86c1-cfabbb95d5f5");

            var productList = new List<ProductModel> {
                new ProductModel(productId1,"Tesla", "Model Y", "Starting at $55,000","Lorem ipsum dolor sit amet, consectetur adipiscing elit.\nFusce porta et augue ut rutrum. Done imperdiet arcu et posuere aliquam. Nam semper ipsum et dolor laoreet molestie.",
                "https://gitlab.com/open-source-delgado/mock-data-and-media/-/raw/main/featureflags/teslay.jpg"),
                new ProductModel(productId2,"2023 Mercedes", "C-Class Cabriolet", "Starting at $57,300","Lorem ipsum dolor sit amet, consectetur adipiscing elit.\nFusce porta et augue ut rutrum. Done imperdiet arcu et posuere aliquam. Nam semper ipsum et dolor laoreet molestie.",
                "https://gitlab.com/open-source-delgado/mock-data-and-media/-/raw/main/featureflags/mercedescabriolet.jpg"),
                new ProductModel(productId3,"TRD Pro", "C-Class Cabriolet", "Starting at $58,000","Lorem ipsum dolor sit amet, consectetur adipiscing elit.\nFusce porta et augue ut rutrum. Done imperdiet arcu et posuere aliquam. Nam semper ipsum et dolor laoreet molestie.",
                "https://gitlab.com/open-source-delgado/mock-data-and-media/-/raw/main/featureflags/toyotatundra.jpg"),
                new ProductModel(productId4,"2023 F-150", "LARIAT", "Starting at $58,000","Lorem ipsum dolor sit amet, consectetur adipiscing elit.\nFusce porta et augue ut rutrum. Done imperdiet arcu et posuere aliquam. Nam semper ipsum et dolor laoreet molestie.",
                "https://gitlab.com/open-source-delgado/mock-data-and-media/-/raw/main/featureflags/ford150.jpg")
            };

            return productList;
        }
    }
}

